package com.example.ujiansim;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class ujianPraktek extends AppCompatActivity {

    VideoView videoView;
    //deklarasi objek

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ujian_praktek);


            getSupportActionBar().setTitle("Simulasi Ujian Praktek");

            videoView = (VideoView) findViewById(R.id.videoView);
            //inisialisasi object videoView
            videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.videoku));
            //digunakan untuk mengidentifikasi resource seperti lokasi video
            videoView.setMediaController(new MediaController(this));
            //menampilkan media controller video
            videoView.start();
            //memulai video
        }
    }






