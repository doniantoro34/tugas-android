package com.example.ujiansim;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class jenis_ujian extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jenis_ujian);
        Button btn2 = (Button) findViewById(R.id.button6);
        btn2.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){
                Intent i = new Intent(jenis_ujian.this,ujianPraktek.class);
                startActivity(i);
            }
        });

        Button btn3 = (Button) findViewById(R.id.button7);
        btn3.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){
                Intent i = new Intent(jenis_ujian.this,alertUjianTheory.class);
                startActivity(i);
            }
        });
    }
}
